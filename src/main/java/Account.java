import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import java.io.Serializable;

@Entity
public class Account implements Serializable
{
    @Id
    @GeneratedValue
    private int accountNr;
    private double balance;
    private String owner;

    /**
     * Oppgave 4
     *
     * De to linjene under, sammen med endringene i AccaountDao.transaction, fikser problemet.
     * Det dukker opp feilmeldinger når jeg prøver å kjøre to transaksjoner i parallell
     */
    @Version
    private int version = 0;

    public Account(int accountNr, double balance, String owner)
    {
        this.accountNr = accountNr;
        this.balance = balance;
        this.owner = owner;
    }

    public Account(double balance, String owner)
    {
        this.balance = balance;
        this.owner = owner;
    }

    public Account() {}

    public void subtract(double value)
    {
        this.balance -= value;
    }

    //Vanlige get- og set-metoder for alle variable og toString

    public int getAccountNr()
    {
        return accountNr;
    }

    public void setAccountNr(int accountNr)
    {
        this.accountNr = accountNr;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @Override
    public String toString()
    {
        return "Account{" + "accountNr=" + accountNr + ", balance=" + balance + ", owner='" + owner + '\'' + '}';
    }
}
