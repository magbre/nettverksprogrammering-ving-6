import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class AccountDao {
    private EntityManagerFactory emf;

    public AccountDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void newAccount(Account a) {
        EntityManager em = getEm();
        try {
            em.getTransaction().begin();
            em.persist(a);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            closeEm(em);
        }
    }

    public List<Account> getAccountsBalanceOver(double value) {
        EntityManager em = getEm();
        try {
            String query = "SELECT OBJECT(a) FROM Account a WHERE a.balance > :value";
            Query q = em.createQuery(query);
            q.setParameter("value", value);
            return q.getResultList();
        } finally {
            closeEm(em);
        }
    }

    public void updateAccount(Account account) {
        EntityManager em = getEm();
        try {
            em.getTransaction().begin();
            Account a = em.merge(account);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            closeEm(em);
        }
    }

    public Account getAccount(int accountNr) {
        EntityManager em = getEm();
        try {
            return em.find(Account.class, accountNr);
        } finally {
            closeEm(em);
        }
    }

    public List<Account> getAllAccounts() {
        EntityManager em = getEm();
        try {
            Query q = em.createQuery("SELECT OBJECT(o) FROM Account o");
            return q.getResultList();
        } finally {
            closeEm(em);
        }
    }

    public void transaction(Account from, Account to, double value) {
        if (value < 0) return;
        EntityManager em = getEm();
        try {
            em.getTransaction().begin();
            from.subtract(value);
            to.subtract(-value);
            em.merge(from);
            try {
                Thread.sleep(10000);
            } catch (Exception e) {
            }
            em.merge(to);
            em.getTransaction().commit();
        }
        catch (Exception e){
            em.getTransaction().rollback();
        }
        finally {
            em.close();
        }
    }

    private EntityManager getEm() {
        return emf.createEntityManager();
    }

    private void closeEm(EntityManager em) {
        if (em != null && em.isOpen()) em.close();
    }

    public static void main(String[] args) {
        /**
         * Oppgave 2
         */
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Account");
        AccountDao accountDao = new AccountDao(emf);
/*
        System.out.println("All accounts before input: " + accountDao.getAllAccounts());

        Account a1 = new Account(10, "Test1");
        Account a2 = new Account(40, "Test2");
        accountDao.newAccount(a1);
        accountDao.newAccount(a2);

        System.out.println("All accounts after input: " + accountDao.getAllAccounts());

        List<Account> largeAccounts = accountDao.getAccountsBalanceOver(20);
        System.out.println("All accounts with balance > 20: " + largeAccounts);

        largeAccounts.get(0).setOwner("Changed owner");
        accountDao.updateAccount(largeAccounts.get(0));

        System.out.println("All accounts after update: " + accountDao.getAllAccounts());*/

        List<Account> accounts = accountDao.getAllAccounts();
        System.out.println("All accounts before transaction: " + accounts);
        accountDao.transaction(accounts.get(1), accounts.get(0), 20);
        System.out.println("All accounts after transaction: " + accountDao.getAllAccounts());
    }
}
